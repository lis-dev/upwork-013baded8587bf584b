<?php

class AccessTokenObtainer
{
    private const URL = 'https://www.googleapis.com/oauth2/v4/token';
    private const GRANTS = 'urn:ietf:params:oauth:grant-type:jwt-bearer';
    private const TOKEN_FIELD = 'access_token';

    public function obtain(string $jwt)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'grant_type' => self::GRANTS,
            'assertion' => $jwt,
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true)[self::TOKEN_FIELD];
    }
}
