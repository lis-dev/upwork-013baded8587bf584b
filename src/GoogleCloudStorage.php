<?php

class GoogleCloudStorage
{
    private const API_URL_POST = 'https://storage.googleapis.com/upload/storage/v1/b/%s/o?uploadType=media&name=%s';
    private const API_URL_GET = 'https://storage.googleapis.com/storage/v1/b/%s/o/%s?alt=media';

    private string $accessToken;
    private string $bucketName;

    public function __construct(string $accessToken, string $bucketName)
    {
        $this->accessToken = $accessToken;
        $this->bucketName = $bucketName;
    }

    /**
     *   curl -X POST --data-binary @OBJECT_LOCATION \
     *       -H "Authorization: Bearer OAUTH2_TOKEN" \
     *       -H "Content-Type: OBJECT_CONTENT_TYPE" \
     *       "https://storage.googleapis.com/upload/storage/v1/b/BUCKET_NAME/o?uploadType=media&name=OBJECT_NAME".
     */
    public function upload(string $filename, ?string $newFilename = null, ?string $contentType = null): array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf(self::API_URL_POST, $this->bucketName, $newFilename ?: pathinfo($filename, PATHINFO_BASENAME)));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($filename));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: '.($contentType ?: mime_content_type($filename)),
            'Authorization: Bearer '.$this->accessToken,
        ]);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    /**
     * curl -X GET \
     *   -H "Authorization: Bearer OAUTH2_TOKEN" \
     *   "https://storage.googleapis.com/storage/v1/b/BUCKET_NAME/o/OBJECT_NAME?alt=media".
     */
    public function download(string $objectName)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf(self::API_URL_GET, $this->bucketName, urlencode($objectName)));
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Authorization: Bearer '.$this->accessToken,
        ]);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
