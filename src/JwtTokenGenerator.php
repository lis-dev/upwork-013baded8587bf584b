<?php

class JwtTokenGenerator
{
    private const SCOPE = 'https://www.googleapis.com/auth/cloud-platform';
    private const AUD = 'https://www.googleapis.com/oauth2/v4/token';

    private string $pathToKeyFile;

    public function __construct(string $pathToKeyFile)
    {
        $this->pathToKeyFile = $pathToKeyFile;
    }

    public function generate(): string
    {
        $privKeyFileData = json_decode(file_get_contents($this->pathToKeyFile), true);
        $jwtHeader = $this->jwtBase64encode(json_encode([
            'alg' => 'RS256',
            'typ' => 'JWT',
            'kid' => $privKeyFileData['private_key_id'],
        ]));
        $jwtClaim = $this->jwtBase64encode(json_encode([
            'iss' => $privKeyFileData['client_email'],
            'sub' => $privKeyFileData['client_email'],
            'aud' => self::AUD,
            'iat' => time(),
            'exp' => time() + 3600,
            'scope' => self::SCOPE,
        ], JSON_UNESCAPED_SLASHES));
        $jwtData = $jwtHeader.'.'.$jwtClaim;
        $privateKey = $privKeyFileData['private_key'];
        $signature = '';
        \openssl_sign($jwtData, $signature, $privateKey, 'SHA256');

        return $jwtData.'.'.$this->jwtBase64encode($signature);
    }

    private function jwtBase64encode($input): string
    {
        return \strtr(\base64_encode($input), ['=' => '', '+/' => '-_']);
    }
}
