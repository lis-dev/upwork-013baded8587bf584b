<?php

use PHPUnit\Framework\TestCase;

class GoogleCloudStorageFunctionalTest extends TestCase
{
    private const ENV_ACCESS_TOKEN = 'ACCESS_TOKEN';
    private const ENV_BUCKET_NAME = 'BUCKET_NAME';

    public function testUpload()
    {
        $gcloud = new GoogleCloudStorage(getenv(self::ENV_ACCESS_TOKEN), getenv(self::ENV_BUCKET_NAME));

        $filename = __DIR__.'/data/testDataFile.txt';
        $newFilename = 'tests/newFile.txt';
        $fileType = 'text/rtf';
        $result = $gcloud->upload($filename, $newFilename, $fileType);
        $this->assertEquals($result['contentType'], $fileType);
        $this->assertEquals($result['name'], $newFilename);

        return $result['name'];
    }

    /**
     * @depends testUpload
     */
    public function testDownload(string $name)
    {
        $gcloud = new GoogleCloudStorage(getenv(self::ENV_ACCESS_TOKEN), getenv(self::ENV_BUCKET_NAME));
        $result = $gcloud->download($name);

        $this->assertStringStartsWith('Lorem ipsum', $result);
    }
}
