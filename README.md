# Basic usage
```php
<?php

require_once './src/GoogleCloudStorage.php';
require_once './src/AccessTokenObtainer.php';
require_once './src/JwtTokenGenerator.php';

// GOOGLE_APPLICATION_CREDENTIALS env variable contains the path to file with credentials
// It set automatically on the Google Cloud Compute Engine
$pathToKeyFile = getenv('GOOGLE_APPLICATION_CREDENTIALS');

// Obtain access_token to make request to API
$jwtTokenGenerator = new JwtTokenGenerator($pathToKeyFile);
$accessKey = (new AccessTokenObtainer())->obtain($jwtTokenGenerator->generate());

// // Upload file
$bucketName = '<YOUR_BUCKET>';
$filename = __DIR__.'/tests/data/testDataFile.txt'; // Path to file that should be uploaded
$newFilename = 'tests/newFile.txt'; // New file name (optional)
$fileType = 'text/rtf'; // File type (optional)

$gcloud = new GoogleCloudStorage($accessKey, $bucketName);
$uploadResult = $gcloud->upload($filename, $newFilename, $fileType);
var_dump($uploadResult); // Check the result
$downloadResult = $gcloud->download($uploadResult['name']);
var_dump($downloadResult); // Raw downloaded files
```

# Functional Tests
1. Copy phpunit.xml.dist to phpunit.xml
2. Set up real env variables for ACCESS_TOKEN and BUCKET_NAME
2. Run `./vendor/bin/phpunit`
